# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of TestExternals.
#
+ Build/Tests/.*
- .*

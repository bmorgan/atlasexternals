# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthSimulationExternals.
#
+ External/CLHEP
+ External/COOL
+ External/CORAL
+ External/Gaudi
+ External/GPerfTools
+ External/Gdb
+ External/Geant4
+ External/VecGeom
+ External/GeoModel
+ External/GoogleTest
+ External/MKL
+ External/PyModules
+ External/dSFMT
+ External/flake8_atlas
+ External/prmon
+ External/yampl
+ External/nlohmann_json
- .*

# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Package for building Lhapdf as part of the offline/analysis release.
#

# The package's name:
atlas_subdir( Lhapdf )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 OLD )
endif()

# The build needs Boost:
if( ATLAS_BUILD_BOOST )
   set( extra_flags "--with-boost=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}" )
else()
   find_package( Boost 1.41 REQUIRED )
   if( BOOST_LCGROOT )
      set( extra_flags "--with-boost=${BOOST_LCGROOT}" )
   endif()
endif()

# ...and Python:
if( ATLAS_BUILD_PYTHON )
   set( Python_EXECUTABLE "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python3" )
else()
   find_package( Python COMPONENTS Interpreter )
endif()

# Declare where to get LHAPDF from.
set( ATLAS_LHAPDF_SOURCE
   "URL;http://cern.ch/lcgpackages/tarFiles/sources/MCGeneratorsTarFiles/LHAPDF-6.5.1.tar.gz;URL_MD5;22cdeb36691e642bdf1fb89d65019ea3"
   CACHE STRING "The source for LHAPDF" )
mark_as_advanced( ATLAS_LHAPDF_SOURCE )

# Decide whether / how to patch the LHAPDF sources.
set( ATLAS_LHAPDF_PATCH "" CACHE STRING "Patch command for LHAPDF" )
set( ATLAS_LHAPDF_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of LHAPDF (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_LHAPDF_PATCH ATLAS_LHAPDF_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/LhapdfBuild" )

# Extra environment options for the configuration.
set( _cflags )
set( _ldflags )
if( CMAKE_OSX_SYSROOT )
   list( APPEND _cflags -isysroot ${CMAKE_OSX_SYSROOT}
                        -I${CMAKE_OSX_SYSROOT}/usr/include )
   list( APPEND _ldflags -isysroot ${CMAKE_OSX_SYSROOT} )
endif()

# Massage the options to make them usable in the configuration script.
string( REPLACE ";" " " _cflags "${_cflags}" )
string( REPLACE ";" " " _ldflags "${_ldflags}" )

# Create the helper script(s).
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh"
   @ONLY )

# Set up the build of LHAPDF:
ExternalProject_Add( Lhapdf
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_LHAPDF_SOURCE}
   ${ATLAS_LHAPDF_PATCH}
   CONFIGURE_COMMAND
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh"
   BUILD_IN_SOURCE 1 )
ExternalProject_Add_Step( Lhapdf forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_LHAPDF_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( Lhapdf buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing LHAPDF into the build area"
   DEPENDEES install )
add_dependencies( Package_Lhapdf Lhapdf )
if( ATLAS_BUILD_BOOST )
   add_dependencies( Lhapdf Boost )
endif()
if( ATLAS_BUILD_PYTHON )
   add_dependencies( Lhapdf Python )
endif()

# Install LHAPDF:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . OPTIONAL USE_SOURCE_PERMISSIONS )
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/lhapdf-sanitizer.cmake.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/lhapdf-sanitizer.cmake"
   @ONLY )
install( SCRIPT
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/lhapdf-sanitizer.cmake" )

# Declare where to get the LHCPDF data files from.
set( ATLAS_LHAPDF_DATA_SOURCE
   "URL;http://cern.ch/atlas-computing/projects/RootCoreExternal/v002/NNPDF23_nlo_as_0118.tar.gz;URL_MD5;139d93c4ed265d227294d8f8c72d6ef8"
   CACHE STRING "The source for the LHAPDF data files" )
mark_as_advanced( ATLAS_LHAPDF_DATA_SOURCE )

# Temporary directory for the data files:
set( _dataDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/LhapdfFiles" )

# Download the LHAPDF data files:
ExternalProject_Add( LhapdfFiles
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_LHAPDF_DATA_SOURCE}
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo
   "No configure needed for LhapdfFiles"
   BUILD_COMMAND ${CMAKE_COMMAND} -E copy_directory "<SOURCE_DIR>/"
   "${_dataDir}"
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory "<SOURCE_DIR>/"
   "<INSTALL_DIR>/share/LHAPDF" )
add_dependencies( Package_Lhapdf LhapdfFiles )

# Install the LHAPDF data files:
install( DIRECTORY "${_dataDir}/"
   DESTINATION "share/LHAPDF" OPTIONAL USE_SOURCE_PERMISSIONS )

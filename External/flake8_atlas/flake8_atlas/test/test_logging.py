# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
from flake8_atlas.test.testutils import Flake8Test

class Test(Flake8Test):
   """
   Test proper use of loggging facilities
   """

   def test1(self):
      """Check lazy logging"""

      # These tests should be checking code like
      #    log.info('logging %s' % 'bad')
      # When run within flake8 it takes care of replacing the actual strings
      # with placeholders to avoid accidental matches (here %). For the tests here
      # we simply omit the string formatter.
      #
      # http://flake8.pycqa.org/en/latest/internal/checker.html#flake8.processor.mutate_string

      self.assertFail("log.info('logging' % 'bad')", 'ATL100')
      self.assertPass("log.info('logging', 'good')", 'ATL100')

      self.assertFail("myfunnyloggername.warning('Hello' % 'world')", 'ATL100')

      self.assertFail("log.info('logging' % ('bad',42))", 'ATL100')
      self.assertPass("log.info('logging', 'good', 42)", 'ATL100')

   def test2(self):
      """Check use of print statements for logging"""
      self.assertFail("print", 'ATL901')
      self.assertFail("print('Hello')", 'ATL901')
      self.assertFail("print 'Hello'", 'ATL901')

      # Those should not trigger warnings:
      self.assertPass("def myprint(): pass", 'ATL901')
      self.assertPass("def printSummary(): pass", 'ATL901')
      self.assertPass("def my_print_function(): pass", 'ATL901')
